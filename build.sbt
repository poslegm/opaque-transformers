val dottyVersion = "0.11.0"

name := "dotty-simple"
version := "0.1.0"

scalaVersion := dottyVersion

libraryDependencies += "com.novocode" % "junit-interface" % "0.11" % "test"
enablePlugins(JmhPlugin)