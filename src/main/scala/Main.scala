package main

import java.util.concurrent.TimeUnit

import scala.concurrent.Future
import scala.concurrent.ExecutionContext
import scala.concurrent.Await
import scala.concurrent.duration._
import scala.util.Try
import org.openjdk.jmh.annotations._

import scala.concurrent.duration._

@State(Scope.Thread)
@BenchmarkMode(Array(Mode.Throughput))
@OutputTimeUnit(TimeUnit.SECONDS)
class Main {
  @Param(Array("1000"))
  var innerValue: Int = _
  
  @Benchmark
  def opaqueTypeTransformerTry(): Unit = {
    implicit val ec = scala.concurrent.ExecutionContext.global
    import transformers._

    val res = for {
      s <- OptionT.create(Try(Some(innerValue))).map(_.toString)
      l <- OptionT.create(Try(Some(s.length))).map(_ + 1)
      d <- OptionT.create(Try(if (l > 2) Some(l - 1) else None))
      x <- OptionT.create(Try(Some(d * 2)))
    } yield x

    res.getOrElse(0).get
  }

  @Benchmark
  def oldTransformerTry(): Unit = {
    implicit val ec = scala.concurrent.ExecutionContext.global
    import oldtransformers._
    val res = for {
      s <- OptionT.create(Try(Some(innerValue))).map(_.toString)
      l <- OptionT.create(Try(Some(s.length))).map(_ + 1)
      d <- OptionT.create(Try(if (l > 2) Some(l - 1) else None))
      x <- OptionT.create(Try(Some(d * 2)))
    } yield x

    res.getOrElse(0).get

  }

  @Benchmark
  def opaqueTypeTransformerFuture(): Unit = {
    implicit val ec = scala.concurrent.ExecutionContext.global
    import transformers._

    val res = for {
      s <- OptionT.create(Future(Some(innerValue))).map(_.toString)
      l <- OptionT.create(Future(Some(s.length))).map(_ + 1)
      d <- OptionT.create(Future(if (l > 2) Some(l - 1) else None))
      x <- OptionT.create(Future(Some(d * 2)))
    } yield x

    Await.result(res.getOrElse(0), 60.second)
  }

  @Benchmark
  def oldTransformerFuture(): Unit = {
    implicit val ec = scala.concurrent.ExecutionContext.global
    import oldtransformers._
    val res = for {
      s <- OptionT.create(Future(Some(innerValue))).map(_.toString)
      l <- OptionT.create(Future(Some(s.length))).map(_ + 1)
      d <- OptionT.create(Future(if (l > 2) Some(l - 1) else None))
      x <- OptionT.create(Future(Some(d * 2)))
    } yield x

    Await.result(res.getOrElse(0), 60.second)
  }

  @Benchmark
  def opaqueTypeEitherTTry(): Unit = {
    implicit val ec = scala.concurrent.ExecutionContext.global
    import transformers._

    val res = for {
      s <- EitherT.create(Try(Right(innerValue))).map(_.toString)
      l <- EitherT.create(Try(Right(s.length))).map(_ + 1)
      d <- EitherT.create(Try(if (l > 2) Right(l - 1) else Left(new Exception("oops"))))
      x <- EitherT.create(Try(Right(d * 2)))
    } yield x

    res.getOrElse(0).get
  }

  @Benchmark
  def oldEitherTTry(): Unit = {
    implicit val ec = scala.concurrent.ExecutionContext.global
    import oldtransformers._
    val res = for {
      s <- EitherT.create(Try(Right(innerValue))).map(_.toString)
      l <- EitherT.create(Try(Right(s.length))).map(_ + 1)
      d <- EitherT.create(Try(if (l > 2) Right(l - 1) else Left(new Exception("oops"))))
      x <- EitherT.create(Try(Right(d * 2)))
    } yield x

    res.getOrElse(0).get
  }

  @Benchmark
  def opaqueTypeEitherTFuture(): Unit = {
    implicit val ec = scala.concurrent.ExecutionContext.global
    import transformers._

    val res = for {
      s <- EitherT.create(Future(Right(innerValue))).map(_.toString)
      l <- EitherT.create(Future(Right(s.length))).map(_ + 1)
      d <- EitherT.create(Future(if (l > 2) Right(l - 1) else Left(new Exception("oops"))))
      x <- EitherT.create(Future(Right(d * 2)))
    } yield x

    Await.result(res.getOrElse(0), 60.second)
  }

  @Benchmark
  def oldEitherTFuture(): Unit = {
    implicit val ec = scala.concurrent.ExecutionContext.global
    import oldtransformers._
    val res = for {
      s <- EitherT.create(Future(Right(innerValue))).map(_.toString)
      l <- EitherT.create(Future(Right(s.length))).map(_ + 1)
      d <- EitherT.create(Future(if (l > 2) Right(l - 1) else Left(new Exception("oops"))))
      x <- EitherT.create(Future(Right(d * 2)))
    } yield x

    Await.result(res.getOrElse(0), 60.second)
  }
}