package main

package object transformers {
  import Monad._
  
  opaque type OptionT[F[_], A] = F[Option[A]]

  object OptionT {
    def create[F[_]: Monad, A](x: F[Option[A]]): OptionT[F, A] = x
    def map[F[_]: Monad, A, B](opt: OptionT[F, A])(f: A => B): OptionT[F, B] = value(opt).map(_.map(f))
    def value[F[_], A](x: OptionT[F, A]): F[Option[A]] = x
    def flatMap[F[_], A, B](opt: OptionT[F, A])(f: A => OptionT[F, B])(implicit M: Monad[F]): OptionT[F, B] =
      value(opt).flatMap(_.fold(M.pure[Option[B]](None))(f))
    def getOrElse[F[_]: Monad, A, B >: A](opt: OptionT[F, A])(default: => B): F[B] = value(opt).map(_.getOrElse(default))
  }

  implicit class OptionTSyntax[F[_]: Monad, A](self: OptionT[F, A]) {
    inline def map[B](f: A => B): OptionT[F, B] = OptionT.map(self)(f)
    inline def flatMap[B](f: A => OptionT[F, B]): OptionT[F, B] = OptionT.flatMap(self)(f)
    inline def getOrElse[B >: A](default: => B): F[B] = OptionT.getOrElse(self)(default)
  }

  opaque type EitherT[F[_], E, A] = F[Either[E, A]]

  object EitherT {
    def create[F[_]: Monad, E, A](x: F[Either[E, A]]): EitherT[F, E, A] = x
    def map[F[_]: Monad, E, A, B](opt: EitherT[F, E, A])(f: A => B): EitherT[F, E, B] = value(opt).map(_.map(f))
    def value[F[_], E, A](x: EitherT[F, E, A]): F[Either[E, A]] = x
    def flatMap[F[_], E, A, B](opt: EitherT[F, E, A])(f: A => EitherT[F, E, B])(implicit M: Monad[F]): EitherT[F, E, B] =
      value(opt).flatMap(_.fold(err => M.pure[Either[E, B]](Left(err)), f))
    def getOrElse[F[_]: Monad, E, A, B >: A](opt: EitherT[F, E, A])(default: => B): F[B] = value(opt).map(_.getOrElse(default))
  }

  implicit class EitherTSyntax[F[_]: Monad, E, A](self: EitherT[F, E, A]) {
    inline def map[B](f: A => B): EitherT[F, E, B] = EitherT.map(self)(f)
    inline def flatMap[B](f: A => EitherT[F, E, B]): EitherT[F, E, B] = EitherT.flatMap(self)(f)
    inline def getOrElse[B >: A](default: => B): F[B] = EitherT.getOrElse(self)(default)
  }
}
