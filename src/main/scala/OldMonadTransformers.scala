package main

import Monad._

package object oldtransformers {
  final case class OptionT[F[_], A](value: F[Option[A]]) {
    def map[B](f: A => B)(implicit M: Monad[F]): OptionT[F, B] = OptionT(value.map(_.map(f)))
    def flatMap[B](f: A => OptionT[F, B])(implicit M: Monad[F]): OptionT[F, B] =
      OptionT(value.flatMap(_.fold(M.pure[Option[B]](None))(a => f(a).value)))
    def getOrElse[B >: A](default: => B)(implicit M: Monad[F]): F[B] = value.map(_.getOrElse(default))
  }

  object OptionT {
    def create[F[_]: Monad, A](x: F[Option[A]]): OptionT[F, A] = OptionT(x)
  }

  final case class EitherT[F[_], E, A](value: F[Either[E, A]]) {
    inline def map[B](f: A => B)(implicit M: Monad[F]): EitherT[F, E, B] = EitherT(value.map(_.map(f)))
    inline def flatMap[B](f: A => EitherT[F, E, B])(implicit M: Monad[F]): EitherT[F, E, B] =
      EitherT(value.flatMap(_.fold(err => M.pure[Either[E, B]](Left(err)), a => f(a).value)))
    inline def getOrElse[B >: A](default: => B)(implicit M: Monad[F]): F[B] = value.map(_.getOrElse(default))
  }

  object EitherT {
    def create[F[_]: Monad, E, A](x: F[Either[E, A]]): EitherT[F, E, A] = EitherT(x)
  }
}
