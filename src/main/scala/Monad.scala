package main

import scala.concurrent.Future
import scala.concurrent.ExecutionContext
import scala.util.Try

trait Monad[F[_]] {
  def pure[A](a: A): F[A]
  def flatMap[A, B](m: F[A])(f: A => F[B]): F[B]
  def map[A, B](m: F[A])(f: A => B): F[B] = flatMap(m)(f andThen pure)
}

object Monad {
  implicit class MonadSyntax[F[_], A](val self: F[A])(implicit M: Monad[F]) {
    def flatMap[B](f: A => F[B]): F[B] = M.flatMap(self)(f)
    def map[B](f: A => B): F[B] = M.map(self)(f)
  }

  implicit def futureMonad(implicit ec: ExecutionContext): Monad[Future] = new Monad[Future] {
    override def pure[A](a: A) = Future.successful(a)
    override def flatMap[A, B](m: Future[A])(f: A => Future[B]) = m.flatMap(f)
  }

  implicit def tryMonad: Monad[Try] = new Monad[Try] {
    override def pure[A](a: A) = scala.util.Success(a)
    override def flatMap[A, B](m: Try[A])(f: A => Try[B]) = m.flatMap(f)
  }

  implicit def optionMonad: Monad[Option] = new Monad[Option] {
    override def pure[A](a: A) = Some(a)
    override def flatMap[A, B](m: Option[A])(f: A => Option[B]) = m.flatMap(f)
  }
}
